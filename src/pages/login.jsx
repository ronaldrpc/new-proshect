import React, { useRef } from "react";
import LoginForm from "../components/login-form";

const Login = () => {
  const inputRef = useRef(null);

  const login = (values) => {
    console.log(values);
  };

  return (
    <div className="container">
      <div className="row">
        <div className="d-flex justify-content-center">
          <div className="col-6 my-5">
            <div className="my-4">
              <h2>Login</h2>
            </div>
            <LoginForm onSubmit={login} innerRef={inputRef} />
            <button
              className="btn btn-primary"
              onClick={() => {
                inputRef.current.submitForm();
              }}
            >
              Login
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
