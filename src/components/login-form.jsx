import { Field, Form, Formik } from "formik";

const LoginForm = ({ onSubmit, innerRef }) => {
  const initialData = {
    username: "",
    password: "",
  };

  const submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetForm();
    actions.setSubmitting(false);
  };

  return (
    <Formik initialValues={initialData} onSubmit={submit} innerRef={innerRef}>
      {({ isSubmitting }) => {
        return (
          <Form>
            <div className="form-group pb-3">
              <div style={{ textAlign: "left" }}>
                <label htmlFor="username">Username</label>
              </div>
              <Field
                className="form-control"
                id="username"
                type="text"
                name="username"
                placeholder="Enter your username"
                //as="text" //convert input into text area
              />
            </div>
            <div className="form-group pb-3">
              <div style={{ textAlign: "left" }}>
                <label htmlFor="pass">Password</label>
              </div>
              <Field
                className="form-control"
                id="pass"
                type="password"
                name="password"
                placeholder="Enter your password"
              />
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

export default LoginForm;
